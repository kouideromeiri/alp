---
layout: page
width: expand
hero:
    title: ALP Yardımına Hoşgeldiniz.
    subtitle: Uyuglama ile ilgili aradığınız bilgilere hızlıca ulaşın
    image: knowledge.svg
    search: true
    author: tanser
---

{% include boxes.html columns="3" title="Browse Topics" subtitle="Chose an option that you need help with or search above" %}

{% include featured.html tag="featured" title="Popular Articles" subtitle="Selected featured articles to get you started fast in Jekyll" %}

{% include videos.html columns="2" title="Video Tutorials" subtitle="Watch screencasts to get you started fast with Jekyll" %}

{% include faqs.html multiple="true" title="Frequently asked questions" category="presale" subtitle="Find quicke answers to frequent pre-sale questions asked by customers" %}

{% include team.html authors="tanser, merve, ayse" title="We are here to help" subtitle="Our team is just an email away ready to answer your questions" %}

{% include cta.html title="Didn't find an answer?" button_text="Contact Us" button_url="/contact/" subtitle="Get in touch with us for details on setup and additional custom services pricing" %}

