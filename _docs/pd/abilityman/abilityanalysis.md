---
title: Yetkinlik Analizi
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden PD-Yetkinlik Yönetimi ile alt kırılımlardan birisi olan *Yetkinlik Analizi* kısmına gelinir.

![yetkinlik](/assets/img/PD/Ability_Man/AbilityAnalysis/abianalysis.png)

**Filtrele** butonu ile bu ekranda filtreleme kart ekranını açılıp; istenilen alanlar filtrelenerek detaylı bir şekilde analiz yapılabilir. Alttaki yüzdelik gösterimlerden mavi renkli olanlar Hedef' e ve pembe renkli olanlar İdeal Hedef'e göre kapanma oranlarını gösterirler. Sağ tarafta ise filtrelenen alanlara göre anlık radar grafiği oluşur. Sağ üst köşede bulunan Details butonu ile hedeflerin Ağırlıklı Hedef Hesaplarını görüntülenebilir.

![yetkinlik_analizi](/assets/img/PD/Ability_Man/AbilityAnalysis/analysis.png)

Polivalans butonuna tıklayarak fitreleme kartında seçilen alanlara göre, kişinin Hedef ve Mevcut seviyeleri polivalans tablosu üzerinde görüntülenebilir. (i) butonuna tıklanıldığında açılan kart ekranında görüldüğü üzere; mavi renkli kutular Hedef, yeşil renkli kutular ise kişinin Mevcut gösterimleridir.

![yetkinlik_analizi](/assets/img/PD/Ability_Man/AbilityAnalysis/abianalysis1.png)