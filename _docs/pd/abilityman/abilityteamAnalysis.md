---
title: Ekip Yetkinlik Analizi
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden PD-Yetkinlik Yönetimi ile alt kırılımlardan birisi olan *Ekip Yetkinlik Analizi* kısmına gelinir.

![ekipyetkinlik](/assets/img/PD/Ability_Man/AbilityTeamAnalysis/abiteamanalysis.png)

Burada seçilen organizasyon(ilgili ekip) ve diğer filtre seçeneklerine göre o ekipte olan herkesin tek tek radar grafikleri ekrana gelecektir. Radar grafiklerin sol üst köşede personelin Hedef ve İdeal Hedef'e göre gösterimleri bulunur. Radarların sağ üst köşesinde bulunan küçük kare simgesine tıklanılarak, radar büyük bir şekilde daha detaylı olarak görüntülenebilir.

![ekipyetkinlik](/assets/img/PD/Ability_Man/AbilityTeamAnalysis/teamabianalysis1.png)

![ekipyetkinlik](/assets/img/PD/Ability_Man/AbilityTeamAnalysis/teamabianalysis2.png)

