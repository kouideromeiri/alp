---
title: Yetkinlik GAP Analizi
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden PD-Yetkinlik Yönetimi ile alt kırılımlardan birisi olan *Yetkinlik GAP Analizi* kısmına gelinir.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/abigap.png)

**Filtrele** butonu ile bu ekranda filtreleme kart ekranı açılıp; istenilen alanlar seçilerek, istenilen kayıtlara ulaşılabilmek için filtreleme yapılabilir.

#### Not 1: 
'E Matris Kapsamında' alanı 'Evet' olarak seçilirse sadece E matris projelerde oluşan Gapleri getirir.

#### Not 2:
'Henüz Planlanmamış' alanı 'Evet' olarak seçilirse, planlanmamış olan Kaizen, Eğitim veya Uygulamalar tabloda yer alacaktır.

Yetkinlik tablosunda her yetkinlik için 4 tane sütun bulunmaktadır.(Seviye 1, 2, 3, 4) Bu sütunlarda yetkinliklerin karşısında bulunan sayılar; ilgili seviyeye geçilebilmesi için o yetkinlikte kaç kişinin olduğunu göstermektedir. (Örn. Hızlı Kaizen yetkinliği için 1.seviyede 2, 2.seviyede 5, 3.seviyede 6 ve 4.seviyede 7 kişi bulunmaktadır.)
İlgili hücreye tıklandığında sağ tarafta bu geçiş ile alakalı kurallar tablosu gelmektedir. İlgili kurala tıklanıldığında alt tabloda bu kuralı yerine getirmesi gereken personeller listenlenmektedir. (1.seviye için Eğitim tipinde 2 personel bulunmaktadır.) Burada ilgili aksiyon üzerinden plan tarihi girebilir. 

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap1.png)

Aşağıdaki ekran görüntüsünde 2.seviye için Kaizen tipinde 5 personel olduğunu göstermektedir. Burada 'Dilan Lale' isimli personelin Kaizen'i planlanmıştır, bu yüzden seçilebilir alanı yoktur. Seviye geçişi planlamak için öncelikle personel seçmek gerekecektir. (Bu kısımda checklist ile birden fazla personel seçilebilir.) Personel veya personeller seçildikten sonra 'Seviye geçişi planla' butonu aktif hale gelecektir.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap2.png)

Aşağıdaki ekran görüntüsünde 3.seviye için Kaizen tipinde 6 personel olduğunu göstermektedir. Burada seviye geçişi sadece 1 personel seçilebilir. (Bu kısımda radio butonu ile bir personel seçilir çünkü takımda sadece 1 tane lider olabilir.) Personel seçildikten sonra 'Seviye geçişi planla' butonu aktif hale gelecektir.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap3.png)

Personeller tablosunun sağ tarafında plan kısmında bulunan linklere tıklandığında ilgili Kaizen(Eğitim yada Uygulama) kart ekranları yan sekmede açılacaktır.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap4.png)

4.seviye için Eğitim tipinde 7 personel bulunmaktadır. Bu kısımda seviye geçişi için radio buton ile sadece bir kişi (sadece bir Eğitmen olabilir) seçilebilir. Yine bu kısımda 'i' butonu ile bir öneri bulunmaktadır. Tıklanıldığı zaman personelin eğitime, eğitmen olarak seçilebileceğini söylemektedir. Eklemek istenirse; personele tıklandığı an seçilen kişiyi bu kısımda eğitmen olarak kataloğa ekler.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap5.png)

Yapılan tüm planlama sonuçlarına göre bir yıllık 'Hedef' ve 'Gerçekleşen' Gap kapatma performansları aylık olarak ortaya çıkar. Sol tarafta ise Gauge grafiği gösterimi yer alır.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap6.png)

Sol üst köşede bulunan **Eğitim Planlarını Tamamla** butona tıklanıldığında ilgili yıla ait tüm eğitim planlarını; eğitmenlere, katılımcılara ve üst amirlerine mail atar.

![gapanalizi](/assets/img/PD/Ability_Man/AbilityGAPAnalysis/gap7.png)

Bu aksiyonlar tamamlandığında personelin mevcudu otomatik olarak güncellenir ve o personel artık ilgili listede yer almaz.

***Kaizen, Eğitim ve Uygulama oluşturma linkleri için tıklayınız


