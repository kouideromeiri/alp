---
title: Yetkinlik Tanımları
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden PD-Yetkinlik Yönetimi ile alt kırılımlardan birisi olan *Yetkinlik Tanımları* kısmına gelinir.

![yetkinlik](/assets/img/PD/Ability_Man/AbilityDefinition/ability.png)

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *Yetkinlik Ayarları* sayfasına gidilir, (+) butonu ile 'Yetkinlik Grup', 'Gecikme Sebebi', 'Yetkinlik Seviyeleri' ve 'Yetkinlik Yaklaşımları' eklemeleri yapılır her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *Yetkinlik Tanımları* ana sayfaya dönülür. 

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/ability1.png)

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/ability2.png)

Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurulur. Aşağıdaki görselde altı kırmızı renkli çizili alanlar zorunlu alanlardır. Yetkinlik Grup WCM seçilirse, pillar ekleme butonu gelecektir. (+) butonu ile birden fazla pillar ekleme yapılabilir. Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir.

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/abi1.png)

Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirildikten sonra 'Yetkinlik Kuralları' ekleme kısımı ve 'Luti Oluştur' butonu aktif hale gelecektir. (+) butonuna tıklanıldığı zaman açılan 'Kural Ekle' kart ekranında eklemeler yapılarak kaydedilir.

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/abi2.png)

Kural eklemede sistemde tanımlı olan seviyeler ve Eğitim, Kaizen, Uygulama olmak üzere 3 çeşit tip bulunmaktadır. Eğitim tipi seçildiğinde Eğitim Kataloğu, Eğitmen/Katılımcı ve Puan zorunlu alanları gelecektir. Bu alanlar doldurulup kaydedildiğinde istenilen seviyede Eğitim tipinde bir geçiş kriteri oluşur.

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/abi6.png)

Kaizen tipi seçildiğinde Kaizen Methodu, Üye/Lider, Kaizen Kaynağı ve 4M Analizi zorunlu alanları gelecektir. Bu alanlar doldurulup kaydedildiğinde istenilen seviyede Kaizen tipinde bir geçiş kriteri oluşmuş olur.

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/abi7.png)

Son olarak Uygulama tipi seçildiği zaman Uygulama ve Puan zorunlu alanları gelecektir. Bu alanlar doldurulup kaydedildiğinde belirlenen seviyede Uygulama tipinde bir geçiş kriteri oluşturulmuş olur.

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/abi8.png)

#### Not 1: 
Aynı seviye için birden fazla geçiş kriteri oluşturulabilir.

#### Not 2: 
Personelin bir seviyeyi geçmesi için birden fazla geçiş kriteri bulunuyorsa(Örn: iki kuralı yerine getirecekse) ve birinde zorunlu tik i işaretli değilse, personel bir kuralı yerine getirdiği an sistem otomatik olarak kişinin yetkinliğini üst seviyeye taşıyacaktır. Her iki kural içinde zorunlu tik i seçili ise,iki kuralda yerine geldiği an kişinin yetkinliği bir üst seviyeye çıkacaktır.

#### Not 3: 
Yeni bir kural eklerken(Örn: eğitim ya da uygulama) eklenilen aksiyon ile ilgili eğitim ya da uygulama katalog olarak tanımlı değilse Ekle butonuna tıklandığında otomatik yeni katalog eklenmiş olur.

'Luti Oluştur' butonu ile LUTI oluşturulabilir.

![ekle](/assets/img/PD/Ability_Man/AbilityDefinition/abi4.png)

**LUTI sayfasına yönlendirilmek için tıklayınız..

Üst kısımda bulunan **Kayıp Yetkinlik İlişkisi** butonuna tıklanarak o sayfaya gidilir. *Kayıp / Yetkinlik İlişkisi* sayfasında yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurulur. Aşağıdaki görselde altı kırmızı renkli çizili alanlar zorunlu alanlardır. Alt tarafta bulunan 'Kayıp Yetkinlik İlişkisi' tablosundan Yetkinlikler seçilir ve kaydet butonu ile kaydedildikten sonra *Geri* butonuyla *Kayıp / Yetkinlik İlişkisi* ana sayfayasına dönülür.

![kayıp_yetkinlik](/assets/img/PD/Ability_Man/AbilityDefinition/abi3.png)

*Kayıp / Yetkinlik İlişkisi* ana sayfasında yenileme butonu ile sayfa yenilenip tablo listelendiği zaman, bir satıra tek kez tıklandığında tüm satır pembe renge boyanacak ve sağ tarafta yeni bir 'Yetkinlikler' tablosu açılacaktır.

![kayıp_yetkinlik](/assets/img/PD/Ability_Man/AbilityDefinition/abi5.png)

Yukarıdaki fotoğrafta, tablolardaki gösterimler; PRES A bölümünde Enerji kaybı yaşanıyorsa ve buna Hızlı Kaizen ile ataklanılacaksa, olması gereken yetkinlikler sağ tarafta açılanlardır.

**Filtrele** butonu ile bu ekranda filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir. Sonrasında *Geri* butonuyla *Yetkinlik Tanımları* ana sayfaya dönülür. Bu sayfada da yine **Filtrele** butonu ile kart ekranınından istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.





