---
title: LUTI
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden PD-Yetkinlik Yönetimi ile alt kırılımlardan birisi olan *LUTI* kısmına gelinir.

![luti](/assets/img/PD/Ability_Man/LUTI/luti.png)

Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurulur. İlk olarak *Luti Konusu* ve *Personel* seçilmelidir. Learn, Use, Teach and Inspect kartları için ayrı ayrı eklemeler yapılır. 'Teach' kartı haricinde diğer üç kart için Açıklama, Tarih ve varsa Ekli dosyalar eklenmelidir. (Bu kısımda ekli dosyalar kısmının aktif, seçilebilir hale gelmesi için kaydetme işlemi gerçekleştirilmedir.) Diğer kartlardan farklı olarak Teach kartında Eğitim de seçilmelidir. Son olarak Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir.

![luti](/assets/img/PD/Ability_Man/LUTI/luti1.png)

![luti](/assets/img/PD/Ability_Man/LUTI/luti2.png)

**Filtrele** butonu ile kart ekranınından istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.