---
title: LLS
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden FI Modülü-LLS kısmına gelinir.

![LLS](/assets/img/FI/LLS/lls.png)

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *LLS Ayarları* sayfasına gidilir, (+) butonu ile 'İyileştirmenin Etkisi', 'Problemin Tipi', 'Problemin Fark Edildiği Safha', 'Problemin Görüntüsü', 'Standartlaştırma Araçları' ve 'LLS Konusu' gibi LLS üzerinde kullanılacak ana verilerin ekleme ve güncellemeleri yapılır her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *LLS* ana sayfaya dönülür.

Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurulur. 'Ekli Dosyalar', 'Onay' ve fotoğraf eklemeye olanak sağlayan 'Öncesi/Sonrası' kısımlarının aktif olması için öncelikle kaydetmek gerekir. (Eğer LLS kaizende oluştululmuşsa ilgili kısımlar otomatik dolu olarak gelecektir.)

![LLS](/assets/img/FI/LLS/lls2.png)

Kaydetme işleminden sonra *Ekli Dosyalar* kısmındaki 'Ekle' butonu aktif hale gelecektir. Buradan (+) butonu ile ilgili dosyaların eklenmesi gerçekleştirilir.

Ekranın sol kısmında 'Anahtar Sözcükler' bölümüne girelen kod (alfanümerik yapıda); aramalar sırasında kolay erişim sağlar.  

![LLS](/assets/img/FI/LLS/lls3.png)

'Onay' kısmında bölüm ve personel seçilerek; bir personele onay gönderilir. Onay verilir ya da reddedilir. 

![LLS](/assets/img/FI/LLS/lls4.png)

Daha sonrasında 'Standartlaştırma Araçları' kısmında artık aktif oldukları için Öncesi/Sonrası fotoğrafları eklenebilir. Sağ tarafta 'Kaizen Bilgileri' kısmında LLS' in geldiği kaizenin bilgileri (eğer LLS Kaizen'de oluşturulmuşsa) bulunur. Bu kısımda bulunan kod ile Kaizen formuna ulaşılabilir. Bir alt kısımda da 'Güncelleme Geçmişi' ile ilgili bilgiler bulunur. Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir. *Geri* butonuyla *LLS* ana sayfaya dönülür. 

![LLS](/assets/img/FI/LLS/lls5.png)

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir ve anasayfada yenile butonuna tıklanarak seçilen kayıtlar listelenebilir. 

