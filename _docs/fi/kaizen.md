---
title: Kaizen
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden FI Modülü-Kaizen kısmına gelinir.

![kaizen](/assets/img/FI/Kaizen/kaizen.png)

Ekrandaki refresh(yenile) butonuyla tüm kaizen listesine ulaşılabilir. **Filtrele** butonu ile çeşitli raporlar alınabilir. Header da sağ kısımda bulunan **Kapanan**,  **Geciken** ve **Silinen Kayıtlar** butonlarından hızlıca kapanan, geciken ve silinen kaizenlerle ilgili rapor listelenebilir. Bu raporlar excele aktarılabilir.
Bu listede tüm kaizenler görüntülenir. (Öneriden kaizene dönen, E matrixten oluşan kaizenler ve spontane oluşan kaizenler)

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *Kaizen Ayarları* sayfasına gidilir, (+) butonu ile 'Kaizen Metodu Takım Kuralları', 'Kaizen ENV Skor', 'Kaizen ISG Skor', 'Kaizen Kazanç Skor', 'Kaizen Maliyet Kalemleri', 'Kaizen Kaynağı', 'Kaizen Standartlaştırma Araçları', 'Kaizen İptal Nedenleri' ve 'Sketch Kalitesi' ekleme ve güncellemeleri yapılır her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *Kaizen* ana sayfaya dönülür.

Yeşil renkli **Ekle** butonuna tıklanarak manuel kaizen ekleme ekranı açılır. Detaylı bir şekilde kaizen bilgisinin girişi yapılabilir. Altı kırmızı renkli çizili alanlar zorunlu alandır. Aynı zamanda kaydet butonu üzerinde görünen (!) işaretinden hangi alanların zorunlu olduğu görülebilir. 'Sorumlu Bölüm' ekip lideri seçildiğinde otomatik dolar. (Sistem 'Sorumlu Bölüm' e Ekip Lider'in bölümünü getirir.)

![kaizen](/assets/img/FI/Kaizen/kaizen2.png)

Kaizen ekibi eklenebilir. Kaizen metodu ile ilişkilendirilmiş yetkinlik beklentisine göre ekip önerir ya da manuel de yönetilebilir. Kaizen ekip oluştururken kural: Lider en az 1 seviyesinde olmalı , Ekip içerisinde de her bir yetkinlikte 3 seviyesinde en az bir kişi bulunmalıdır. Program liderlik yapabilecek personeli önerir. (4 yetkinliği sağlayan Özkan Şener'i lider olarak önermiştir.) Aşağıdaki görselde gösterilmiştir. Önerilen personelin isminin yan tarafında bulunan (+) butona tıklanarak lider eklenebilir. Eğer önerilen personel bulunmuyorsa 'Manuel Seçim' kısmından da lider ekleme yapılabilir.

![kaizen](/assets/img/FI/Kaizen/kaizen3.png)

Program ekibe üye personel önerisinde de bulunur. (Kaizen ekip kuralı gereği 4M Analysis yetkinliği için ekipten en az 1 kişinin 3 seviyesinde olması gerekmektedir.) Aşağıdaki görselde gösterilmiştir. Üye olarak önerilen personelin isminin yan tarafında bulunan (+) butona tıklanarak üye eklenebilir. Eğer önerilen personel bulunmuyorsa 'Manuel Seçim' kısmından da üye ekleme yapılabilir.

![kaizen](/assets/img/FI/Kaizen/kaizen4.png)

Yine 'Kaizen Metodu' kısmında tanımlanan 'FI Araçları' method seçildiği an otomatik olarak gelir. (+) butonu ile çoğaltılabilir ya da azaltılabilir. Aşağıdaki görselde sarı renkli kutucuklar içerisindeki alanlar; bölümün (PRES A) otomatik olarak liderin(Özkan Şener) bölümünden geldiğini göstermektedir. Son olarak personel profillerinin etrafında bulunan çerçevelerin rengi personelin yaka rengini gösterir. (Aşağıdaki örnekte lider ve üyenin raka renklerinin beyaz olduğu görülmektedir.)

![kaizen](/assets/img/FI/Kaizen/kaizen5.png)

Kaizen ekle kısmı güncellenebilir. Saha çalışmalarından sonra gelen veriler tek seferde sisteme girilebilir ya da sahadan formlar geldikçe sisteme girilerek bu kısım güncellenebilir. (Örn: Kaizen ekle ile, kaizen yaratılarak; form için kaizen numarası alınıp, sistemde zorunlu alanlar doldurulduktan sonra kaydedilir ve sahadan veri geldikçe sisteme girilebilir.)

Çalışma alanı kaizenin yapılacağı yerle ilgili bilgiler girilebilir. Bu kısımda bulunan 'Bölüm' Kaizen'in nerede yapıldığını göstermektedir.

Ekranın sağ tarafında 'PDCA' kısmında kapanış tarihi yer alır, o kısımda manuel olarak giriş yapılamaz, tüm işlemlerden sonra 'Kaizeni Kapat' butonuna tıklanıldığı zaman küçük bir kart ekranı gelir. Gelen ekranda girilen tarihi Kaizen'nin kapanış tarihi olarak atar. Kaizen kapatıldıktan sonra kapanış tarihinden itibaren F matrise kazanç oluşturmaya başlayacaktır.

Yine ekranın sağ tarafında 'Finans Bilgileri' kısmında 'F Matris Kapsamında' tik i seçili olarak gelir, eğer işaret kaldırılırsa sonuçlar F matrise düşmeyecektir. 'Kazanç Ay Sayısı' varsayılan 12 ay olarak gelir fakat ay sayısı kısmında değişiklikler yapılabilir.

![kaizen](/assets/img/FI/Kaizen/kaizen6.png)

'İyileştirme Bilgisi' kısmında *Çevre*, *ISG* ve *Kazanç* olmak üzere üç farklı iyileştirme yöntemi bulunur ve bu butonlara tıklanarak Çevre ve ISG risk puanları azaltılabilir ya da Kazanç ortaya çıkarılabilir. Çevre veya ISG kaizeni ise butonlara tıklanarak ilgili bilgiler doldurulur. Sisteme girilmiş risk kaynakları seçilerek öncesi ve sonrası risk puanları girilerek fark hesaplaması elde edilir.

![kaizen](/assets/img/FI/Kaizen/kaizen7.png)

### ISG

**************ISG tanımları için tıklayınız..

![kaizen](/assets/img/FI/Kaizen/kaizen8.png)

Bir kaybın ne kadar iyileştirildiği, ne kadar kazanç sağladığı girişi yapılır. Kayıp İyileştirme Ekle butonuna tıklanarak CD tarafında tanımlanmış kayıp bilgileri seçilir. Sistem girdiğimiz verilere göre otomatik hesaplama yapabilir fakat (+) butonuna tıklayarak el ile sağlanan kazançlar girilebilir. Finansal çıktısı seçilerek toplam tutar yazılır ve kaydedilir. Birden fazla kazanç girilebilir.

![kaizen](/assets/img/FI/Kaizen/kaizen9.png)

![kaizen](/assets/img/FI/Kaizen/kaizen10.png)

![kaizen](/assets/img/FI/Kaizen/kaizen11.png)

![kaizen](/assets/img/FI/Kaizen/kaizen12.png)

'Uygulama Maliyeti' kısmında bu kaizeni yaparken ortaya çıkan maliyet girilir. Maliyet tipi seçilerek, açıklamasını yaparak toplam tutarı yazılır. Bu kısım kaizenin kaydedilebilmesi için zorunlu bir alandır. Amortisman kapsamında olan projeleri ayrıştırmak için 'Amortisman Kapsamında' tiki seçilebilir.

'Kaizen Standartlaştırma Araçları' (+) butonundan eklenebilir. 

Kaizen kaydedildikten sonra Kaizen Yaygınlaştırma, Ekli Dosyalar ve otomatik LLS oluşturma butonları aktif olacaktır. Ekle butonu ile istenilen belgeler sisteme eklenebilir. Kaizen formu taranarak eklenebilir. LLS oluştur butonuyla otomatik LLS oluşur. (Kaizenin bilgileri kullanılarak otomatik LLS oluşturulur ve kod verilir. Bu kodun linki ile direk ilgili LLS'e ulaşılabilir.)

![kaizen](/assets/img/FI/Kaizen/kaizen13.png)

Kaizen Yaygınlaştırma ile kaizeni hangi bölümde hangi personel yaygınlaştırdı ya da yaygınlaştıracak ise sisteme bunun girişi yapılabilir. Sistem bunu sadece bilgi olarak saklar. Yeni bir kazanç üretmez. Toplam kazanç burada girdiğimiz kaizen kazancı olarak kalıyor. Diğer yaygınlaştırılan yerde de bir kaizen kazancı olsun istenilirse bir kaizen daha oluşturulabilir ya da mevcut kaizenin kazancı arttırılabilir.

Benzer Kaizenler; girilen bilgilere göre tekrar eden kaizenleri tespit etmek içindir. Mükerrer kayıt olmasını engeller.

**Kaizeni Kapat** butonu ile listeden açık bir kaizene çift tıklanarak kart içine girilir. Ekranın sağ tarafında 'Kaizen Ekibi' alt tarafında kaizenin 'Durumu' görülebilir. Kaizeni kapat butonuna tıklanır, kapanış tarihi girilerek kaizen kapatılır. Kaizen'i kapatmak için 'Çözüm Açıklaması' girmek zorunludur. Kaizen başarılı bir şekilde kapatılırsa, o kaizende yer alan tüm personellerin yetkinlikleri otomatik olarak güncellenir, artar.

![kaizen](/assets/img/FI/Kaizen/kaizen14.png)

![kaizen](/assets/img/FI/Kaizen/kaizen15.png)

**Kaizeni Aç** butonu ile kapalı bir kaizene girilerek kaizeni aç butonuna tıklayarak kaizen açılabilir. *Geri* butonuyla *Kaizen* anasayfaya dönülür.

Geciken kaizenler
Geciken kaizenler filtreden seçilerek listelenir. Açık olan kaizene çift tıklanarak kaizen kartına girilir ve üst kısımda kaizen gecikmiştir butonuna tıklanarak gecikme sebebi girilir. Sonra kaizen kapatılır veya iptal edilir.

******( Kaizen gecikme nedenleri tanımları için link verilecek.)

*Kaizen* anasayfada **Analiz** butonu ile **Skor Analiz**, **Personel Analiz** ve **Bölüm Analiz** alt butonları ile ilgili analizlere ulaşılabilir. **Skor Analizi** butonu ile *Kaizen Skor Analizi* anasayfasından (refresh butonuna tıklamak gerekli) kaizen skorlarına göre analiz elde edilir. **Personel Analiz** butonuna tıklanarak *Kaizen Personel Analizi* anasayfasından personellerin katılım sağladığı kaizen metodlarına göre analiz elde edilir. **Bölüm Analizi** butonuna tıklandığında *Kaizen Bölüm Analizi* anasayfasından bölüm bazında yapılan kaizen metodları analizi elde edilir.






