---
title: FI Araçları
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden FI Modülü-FI Araçları kısmına gelinir.

![FI_Araçları](/assets/img/FI/FI_Tools/tool.png)

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *FI Araçları Ayarları* sayfasına gidilir, (+) butonu ile 'FI Arac Seviyeleri' ve 'FI Araç Grupları' eklemeleri yapılarak her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *FI Araçları* ana sayfaya dönülür.

Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurulur. Aşağıdaki görselde altı kırmızı renkli çizili alanlar zorunlu alanlardır. Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir. 

![FI_Araçları](/assets/img/FI/FI_Tools/tool2.png)

Kaydetme işleminden sonra 'Kaizen Metodu' ve 'Yetkinlik' ekleme kısımları aktif hale gelecektir. (+) butonu ile birden fazla kaizen metodu ve yetkinlik eklenebilir. Sonrasında *Geri* butonuyla *FI Araçları* ana sayfaya dönülür.

![FI_Araçları](/assets/img/FI/FI_Tools/tool3.png)

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.



