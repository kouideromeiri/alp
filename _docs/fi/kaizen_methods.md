---
title: Kaizen Methodları
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
--- 

Sol taraftaki pillar seçeneklerinden FI Modülü-Kaizen Methodları kısmına gelinir.

![Kaizen_Methodları](/assets/img/FI/Kaizen_Methods/method.png)

Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurulur. Aşağıdaki görselde altı kırmızı renkli çizili alanlar zorunlu alanlardır. 'D Matris kapsamında' tiki işaretlenirse, bu bölüm D Matris kapsamında yer alacaktır. Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir. 

![Kaizen_Methodları](/assets/img/FI/Kaizen_Methods/method2.png)

Kaydetme işleminden sonra 'FI Araçları' ve 'Yetkinlik' ekleme kısımları aktif hale gelecektir. (+) butonu ile birden fazla FI aracı ve yetkinlik eklenebilir. Sonrasında *Geri* butonuyla *Kaizen Methodları* ana sayfaya dönülür.

![Kaizen_Methodları](/assets/img/FI/Kaizen_Methods/method3.png)

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.
