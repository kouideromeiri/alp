---
title: E Matris
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-E Matris kısmına gelinir.

![EMatrix](/assets/img/CD/E_Matrix/ematrix.png)

D matristen projeler seçilip buraya aktarılmıştı. Aktarım işleminden sonra aktarılan projelerin, takvim üzerinde hangi zaman aralıklarında gerçekleşeceği planlanmalı, netleştirilmelidir. *E MATRİS* ana sayfada **GANTT Şeması** butonuyla *E MATRIS - GANTT* sayfasına ulaşılır. Bu sayfada her projenin hangi aylar arasında yapılacağı belirlenir. Kaizen methodları(FI modülünde) tanımlanırken; tahmini proje süresi giriliyor. Buradan gelen süreye göre sistem otomatik olarak, takvimin ilk ayına bu değerleri atar. Daha sonra takvim üzerinde değişiklikler yapılarak projelerin başlangıç ve bitiş tarihleri güncellenebilir. Güncelleme işlemin gerçekleştirilebilmesi için sol üst köşede bulunan butonlardan birisi olan **Değişiklik Modu** butonuna tıklanarak Kapalı ise Açık duruma getirmek gerekir. Takvimde yapılan değişikliklere göre sol alt kısımda bulunan ve Perimeter hedefe ne kadar yaklaşıldığnı gösteren Gauge grafiği sürekli güncelleniyor. Bu grafikteki turuncu alan bir önceki yıldan sarkan kazançları gösterir. Bu gösterimin hemen yan tarafında ise; Kaizen'i gerçekleştirecek liderleri gösteren tablo yer alır. Burada yeşil renge boyalı hücreler; personelin mühendislik seviyesine göre gerçekleştirebileceği seviyede bir proje planlandığını gösterir(tam doluluk). Kırmızı renkli gösterimler; seviyesinin üstüne olduğunu(aşırı doluluk) ve son olarak gri renkli gösterimler ise; seviyesinin altında(projeye dahil olabileceğini) projeler planlandığını ve o aylara planlamalar yapılabileceğini gösterir. Sisteme önceden girilen değerlere/parametrelere göre bu veriler yani personelin aylık yük dağılımları burada oluşur. Yine burada mouse(fare) ile ilgili hücrenin üzerine gelindiği zaman; E Matrix ve onun haricinde oluşan bir Spontane Kaizen varsa, ilgili ay için daha sağlıklı bir planlama yapılabilmesi adına her iki değer de ilgili aya yansıyarak gösterilir. Ekranın en alt kısmında ise; 'Bölümlere Göre Dağılım(Tutar)' ve 'Bölümlere Göre Dağılım(Proje Adedi)' olmak üzere karar vermeyi kolaylaştıran pasta grafiği gösterimleri, detaylı analiz yapılabilmesini sağlayan 'E Matris Yıllık Analiz' ve 'E Matris Gecikme Nedenleri Analizi' grafikleri yer alır.

Aynı sayfada sol üst kısımda bulunan **Proje Planlarını Tamamlama** butonuna tıklanıldığı zaman; ilgili yıla ait tüm proje planlarını, proje liderlerine ve üst amirlere mail olarak gönderir. 

Yine bu sayfada **Filtrele** butonu ile filtreleme kart ekranını üzerinden filtreleme yapılarak istenilen kayıtlara ulaşılabilir. *Geri* butonuyla *E MATRIS* ana sayfaya dönülür.

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *E MATRİS Ayarları* sayfasına gidilir, (+) butonu ile 'Proje Kaynağı' tablosuna eklemeler yapılır(D Matris, Lojistik, Enerji vs) her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *E MATRIS* ana sayfaya dönülür.

D matrisden aktardığımız projelerden bir tanesinin üzerine çift tıklanarak içerisine girildiği zaman, D matris tarafında girilen detaylar burada görülür ve üzerinde değişiklikler yapılmak istenirse ekleme ve güncellemeler yapılabilir. 

D matrisden aktarılan projeler dışında burada yeni projelerde oluşturulabilir. Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurularak kaydedilir. Altı kırmızı renkli çizili alanlar zorunlu alanlardır. Bitiş tarihi; seçilen kaizen methoduna göre otomatik atanabilir fakat projenin bitmesi öngörülen bir tarih varsa tarih kısmı güncellenebilir. Zorunlu alanlardan bir tanesi olan -Kaizen Methodu- seçildiği zaman sağ tarafta bulunan FI Araçları otomatik olarak sisteme gelirler. Bu kısımda FI Araçlarına ekleme ya da çıkarma yapılabilir. Kaydetme işleminden sonra 'Kaizen Oluştur' ve 'Ekle' butonları aktif hale geleceklerdir.

![EMatrix](/assets/img/CD/E_Matrix/ematrix1.png)

Daha sonra ekip oluşturulmaya başlanır. Sağ tarafta Kaizen Ekibi kısmında bulunan kaleme tıklanır. Kaizen Metodu, FI Araçları ve Kayıp Tanımı ile ilişkilendirilmiş yetkinlik beklentisine göre sistem ekip önerir ya da manuel de yönetilebilir. Kaizen ekip oluştururken kural: Lider en az 1 seviyesinde olmalı , Ekip içerisinde de her bir yetkinlikte 3 seviyesinde en az bir kişi bulunmalıdır. Program liderlik yapabilecek personeli önerir. Aşağıdaki görselde gösterilmiştir. Önerilen personelin isminin yan tarafında bulunan (+) butona tıklanarak lider eklenebilir. Eğer önerilen personel bulunmuyorsa 'Manuel Seçim' kısmından da lider ekleme yapılabilir. Ekibe ilk olarak lider eklenmesi gerekir. Daha sonra ekip üyeleri eklenir. 

![EMatrix](/assets/img/CD/E_Matrix/ematrix2.png)

Program ekibe üye personel önerisinde de bulunur. Üye olarak önerilen personellerin isimlerinin yan taraflarında bulunan (+) butona tıklanarak üye eklenebilir. Eğer önerilen personel bulunmuyorsa 'Manuel Seçim' kısmından da üye ekleme yapılabilir. Ekipten bir üye çıkarılmak istenilirse, profilinin üzerine gelinerek açılan bilgilendirme kart ekranında kırmızı renkli 'Ekipten Çıkart' butonuna tıklanarak üye ekipten çıkarılabilir. Personel profillerinin etrafında bulunan çerçevelerin rengi personelin yaka rengini gösterir. 

![EMatrix](/assets/img/CD/E_Matrix/ematrix3.png)

![EMatrix](/assets/img/CD/E_Matrix/ematrix4.png)

Kazanç kısmında Kazanç Ay Sayısını 12 ay olarak getirir fakat projenin daha kısa sürede bitmesi öngörülüyorsa, bu kısıma ekleme yapılarak güncellenebilir. Tüm işlemler yapılıp kaydedildikten sonra ilgili yıldaki E matris projeleri için kaizen oluşturmak gerekir. Kaizen oluşturmak istenilen kayıtların içerisine girilerek 'Kaizen Oluştur' butonuna tıklanarak FI modülü tarafı için tek tuşla kaizen ya da E matristen planlanan değerlerle birlikte F matris için kayıtlar oluşturulması sağlanabilir.

Burada kaizen oluşturulunca FI modülü tarafında kaizenler oluşup; FI süreci yönetildikçe, F matris otomatik olarak güncellenecektir.













