---
title: A Matris
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-A Matrix kısmına gelinir.

![AMatrix](/assets/img/CD/A_Matrix/amatrix.png)

Ekrandaki refresh(yenile) butonuyla tüm A Matris listesine ulaşılabilir. **Filtrele** butonu ile istenilen yılın kayıpları filtrelenebilir. 

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *A MATRIS Ayarlar* sayfasına gidilir, (+) butonu ile 'Risk Seviyesi' ve Min/Max  yüzde ağırlıkları ile renk aralık girişleri yapılır.(Yeşil/Sarı/Kırmızı) Her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *A MATRIS* ana sayfaya dönülür.

![AMatrix](/assets/img/CD/A_Matrix/amatrix5.png)

Ekranın üst kısımdaki araç çubuğunda sağ tarafta A Matris **Nitel** ve **Nicel** gösterim butonları bulunur. Nitel A Matris; personelin tecrübe olarak girdiği kayıpların, öngörülen etkilerinin yönetilebileceği sadece renkli gösterimli kısımdır.

![AMatrix](/assets/img/CD/A_Matrix/amatrxnitel.png)

Nicel A Matris yıl içerisinde C Matrisden toplanan verilerle oluşan gerçek matrisdir. Renkli gösterime ek olarak sayısal veri gösterimleri de bulunur. Nicel A Matris'e tıklanıldığı zaman hemen yan tarafında **Hesapla** butonu gelir. Bu butona tıklanıldığında son sütundaki (Tüm Fabrika) veriler güncellenir(hesaplanır). Her bir satır için tüm verilerin toplamını son sütunun ilgili satırında gösterir.

![AMatrix](/assets/img/CD/A_Matrix/amatrxnicel.png)

Kayıplar arasında daha dengeli bir renklendirme yapılabilmesi için sistem her kayıp başlığını(satırını) kendi içinde bir ağırlık yüzdesi ile renklendiriyor. Son sütun (Tüm Fabrika ise kendi içerisinde farklı bir oranlama ile renklendiriliyor.

Bu kısımlar A Matris'in analiz gösterimleridir. A Matris aynı zamanda kayıpları oluşturulduğu, yeni bir kayıp ortaya çıktığı an bu kaybın eklendiği kısımdır.

Sol üst köşede bulunan **Kayıp Tanımları** butonuna tıklanarak *Kayıp Tanımı* sayfasına ulaşılır ve bu sayfada sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *Kayıp Yetkinlik Ayarları* sayfasına gidilir, (+) butonu ile 'Finansal Çıktılar' ve 'Kayıp Grubu' tablolarına eklemeler ya da kayıtlar üzerinde güncellemeler yapılır. 'Finansal Çıktılar' tablosunda Hesaplamalar kısmında dropdown menüden bir seçim yapmak gerekir. (Hesaplamalar için sisteme gömülü tanımlı formüller bulunur ve doğru formülün bulunabilmesi için burada kayıp isminin seçilmesi gerekir.) Kaydetme işleminden sonra *Geri* butonuyla *Kayıp Tanımı* sayfasına dönülür.

![AMatrix](/assets/img/CD/A_Matrix/amatrix1.png)

Yeşil renkli **Ekle** butonu ile açılan sayfada ilgili kısımlar doldurulur. Altı kırmızı renkli çizili alanlar zorunlu alanlardır. Tüm girişlerden sonra kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir.

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.

Kayıp Tanımı anasayfada sol kısımda bulunan tree list, kayıpları kategorize ederek kayıplara daha kolay ulaşılabilmesini sağlar. Bir kaybın üzerine tıklandığında o kayıbın detayı ve alt kayıpları yan tarafta bulunan tabloda listelenir. Kayıba çift tıklanarak içerisine girilebilir. '+' ikonuna tıklanıldığı zaman yan sekmede 'Kayıp Tanımları' sayfası açılır. Altı kırmızı renkli çizili alanlar zorunlu alanlardır. Zorunlu alanları girdikten sonra kaydet butonu aktif hale gelecektir. Bu sayfadan yeni kayıp eklenip, kaydedilebilir.

![AMatrix](/assets/img/CD/A_Matrix/amatrix2.png)

![AMatrix](/assets/img/CD/A_Matrix/amatrix3.png)

Gruplandırılmış kayıpların alt kırılımı olan kayıpların yan tarafında 'kalem' ikonuna tıklanırsa yine yan sekmede açılan sayfada gerekli düzenleme işlemleri yapılarak kaydedilebilir. 

![AMatrix](/assets/img/CD/A_Matrix/amatrix4.png)

Kayıp Tanımı anasayfada sol üst köşede bulunan **Geri** butonuyla *A Matris* ana sayfasına dönülür.

Sol üst köşede bulunan **Kayıp Yetkinlik İlişkisi** butonuna tıklanarak *Kayıp Yetkinlik İlişkisi* sayfasına ulaşılır ve bu sayfada sol üst köşede bulunan yeşil renkli **Ekle** butonu ile açılan sayfada ilgili kısımlar doldurulur. Altı kırmızı renkli çizili alanlar zorunlu alanlardır. Daha sonra kayıp alt kısımda bulunan tablodan bir yetkinliğe bağlanır. Tüm girişlerden sonra kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir. 

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.

Sonrasında *Geri* butonuyla *A Matris* ana sayfasına dönülür.
