---
title: D Matris
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-D Matris kısmına gelinir.

![DMatrix](/assets/img/CD/D_Matrix/dmatrix.png)

D Matris in oluşabilmesi için C matriste toplanan kayıpların buraya getirilmesi(dönüştürülmesi) gerekir. Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurularak kaydedilir. Hangi yıl olacağı, kaç ayın dikkate alınacağı ve minimum tutar girilip aktarım yapılabilir. 

Birleştirme ile; bir proje başlığı altında birden fazla kayıp birleştirilerek tek seferde çözümlenebilir. Aktarım işleminden sonra gelen kayıtlar arasında örneklendirme adına; Bölüm bazında -PRES A- seçerek bir filtreleme yapıyoruz. Buradan iki kaydı birleştirerek tek bir proje başlığı altında toplamak için iki kayıt seçip **Birleştir** butonuna tıklayıp açılan kart ekranında ilgili kısımlar doldurulduktan sonra kaydedilir.

![DMatrix](/assets/img/CD/D_Matrix/dmatrix1.png)

![DMatrix](/assets/img/CD/D_Matrix/dmatrix2.png)

Birleştirme işleminden sonra ekranın sağ üst köşesinde "D Matris başarılı ile birleştirilmiştir." şeklinde bir bildirim gelecektir. Bu bildirimde yeni D Matris kodu da bulunur.  (v) sembolünün olduğu satır birleştirilmiş yeni satırdır. Aşağıdaki görselde sembole tıklanıldığı zaman açılan tabloda ilgili satırı oluşturan alt başlıklar gösterilmiştir.

![DMatrix](/assets/img/CD/D_Matrix/dmatrix3.png)

İlgili satıra çift tıklanıldığı zaman D Matrix detay kart ekranına ulaşılır. En üst satırda kaybın nerede oluştuğu ile ilgili bilgiler bulunur. Bir alt kısımda -Üretim Miktarı(Bu Yıl)- otomatik geliyor ve bir sonraki yıl için -Üretim Miktarı(Gelecek Yıl)- ı ve  QA (kalite) kısımındaki değişim öngürüsünü sisteme giriyoruz. Yine bu kısımda -Kazanç Tip- i zorunlu alandır. ICES'i ortaya çıkan puan skalaları ayarlar kısmında belirleniyor. İlgili tüm alanlar girildikten sonra kaydedilir. Kaydetme işleminden sonra aynı kayıt tekrar açıldığında ICES puanına gelen değerler görülebilir.

![DMatrix](/assets/img/CD/D_Matrix/dmatrix4.png)

![DMatrix](/assets/img/CD/D_Matrix/dmatrix5.png)

Toplu Güncelleme ile; istenilen alana göre yine istenilen kayıtlar seçilerek hepsi aynı anda güncellenebilir. Mesela belirli bir bölümde ortalama bir iyileştirme oranı olabilir. Makine ya da hat ile alakalı tüm kayıtlara bir kaizen ile ataklanılmak istenilebilir. Filtre atıldıktan sonra satır satır seçilerek, eğer filtreleme uygunsa toplu seçim ile tüm kayıtlar seçilebilir. Ya da toplu seçimden sonra istenilen satırlar seçimden çıkarılarak **Toplu Güncelleme** butonuna tıklanır ve gelen kart ekranında istenilen kısımlar doldurulduktan sonra işlem gerçekleştirilebilir. Örneklendirme adına; ilk olarak Bölüm bazında -BOYA1- seçerek bir filtreleme yapıyoruz. Gelen kayıtların(23 kayıt) hepsini seçerek **Toplu Güncelleme** butonuna tıklayıp gelen kart ekranında -İyileştirme Oranı- kısmına ekleme yapıyoruz. % 10 oranında bir iyileştirme öngördüğümüzü girip ve -Major Kaizen Method- u seçerek kaydediyoruz. Kaydetme işleminden sonra "D Matris başarılı ile güncellenmiştir." mesajı gelip, kart ekranda girdiğimiz değerlere göre tüm seçili kayıtlar güncellenecektir.

![DMatrix](/assets/img/CD/D_Matrix/dmatrix6.png)

![DMatrix](/assets/img/CD/D_Matrix/dmatrix7.png)

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *D MATRİS Ayarları* sayfasına gidilir, (+) butonu ile 'ICES I', 'ICES C' ve 'Hard/Soft Ayarları' tablolarına eklemeler yapılır her biri sol kısımdaki tik ile kaydedilir. Sonrasında *Geri* butonuyla *D MATRIS* ana sayfaya dönülür.

D Matris tarafında yapılabilecek son işlem projelerin E matrise aktarılması işlemidir. Bu kısımda amaç; toplam D matris listesinde bulunan tüm kayıplara ataklanmak ve E matristeki hedefe ulaşabilmektir. Sistemde en alt satırda -Hedef(Perimeterden gelen)-, -Toplam İyileştirme- ve -Toplam Maliyet- bulunur. E matrise aktarmaya başlamadan önce ilk olarak İyileştirme Maliyeti'ni- kontrol etmek gerekir. Bu değer toplam -Hedef- değerinden büyük olmalıdır. 

Aktarmak istenilen satırlar seçildikten sonra **E Matris'e Aktar** butonuna tıklanır. Gelen kart ekranında sistem küçük bir istatistik gösterip onay isteyecektir. Evet olarak seçildikten sonra aktarma işlemi gerçekleşmiş olacaktır. D matris ekranında sarı renge boyanan satırlar aktarılan satırları gösterir. Filtrele ekranında da bunun filtresi atılabilir.

![DMatrix](/assets/img/CD/D_Matrix/dmatrix8.png)

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.





