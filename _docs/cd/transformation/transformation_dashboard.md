---
title: Transformasyon Anasayfa
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-Transformasyon ile alt kırılımlardan birisi olan *Transformasyon Anasayfa* kısmına gelinir.

![transformasyon](/assets/img/CD/Transformation/TransformationDashboard/transform.png)

Transformasyon anasayfasında ilk olarak mavi renkli **Filtrele** butonuyla yılı seçmek (filtrelemek) gerekir. Seçimden sonra 3 ayrı pasta grafik gösterimi ve alt kısımlarında ilişkili oldukları tablolar ekrana gelecektir. İlk tabloda tüm fabrikanın bölümleri yer alır. Bir bölüm seçilmek istenirse üzerine tıklanır ve ilgili satır pembe renge boyanır ve hemen yan tarafında bulunan tabloda seçime göre ilgili 'Masraf Kalemleri' açılır. Yine bu tablodada aynı işlemler yapılarak yan tarafta 'Masraf Detayı' tablosuna ulaşılabilir. Böylelikle giderler daha detaylı olarak analiz edilebilir.

![transformasyon](/assets/img/CD/Transformation/TransformationDashboard/transformasyon.png)

![transformasyon](/assets/img/CD/Transformation/TransformationDashboard/transformasyon1.png)



