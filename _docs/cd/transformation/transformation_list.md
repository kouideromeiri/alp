---
title: Transformasyon Liste
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-Transformasyon ile alt kırılımlarından birisi olan *Transformasyon liste* kısmına gelinir.

![transformasyon](/assets/img/CD/Transformation/TransformationList/transform2.png)

Sol üst köşede bulunan **Ayarlar** butonuna tıklanarak *Transformasyon Ayarları* sayfasına gidilir, (+) butonu ile 'Masraf Kalemi' tablosuna ekleme ve güncellemeler yapılır her biri sol kısımdaki tik ile kaydedilir. Eklenen yeni kayıta detay eklenmek isternirse; ilgili satıra tıklanıldığında sağ tarafta bulunan 'Masraf Kalemi' tablosu aktifleşecektir. Bu tablo üzerinde de (+) butonu ile ekleme ya da kayıtlar üzerinde güncellemeler yapılabilir. Sonrasında *Geri* butonuyla *Transformasyon* ana sayfaya dönülür.

![transformasyon](/assets/img/CD/Transformation/TransformationList/transform3.png)

![transformasyon](/assets/img/CD/Transformation/TransformationList/transform4.png)

Yeşil renkli **Ekle** butonuna tıklanarak ekleme ekranı açılır. Gelen ekranda ilgili kısımlar doldurulur. Altı kırmızı renkli çizili alanlar zorunlu alandır. Aynı zamanda kaydet butonu üzerinde görünen (!) işaretinden hangi alanların zorunlu olduğu görülebilir. Kaydet butonuna tıklanarak kaydetme işlemi gerçekleştirilir.

![transformasyon](/assets/img/CD/Transformation/TransformationList/transform5.png)

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir.
