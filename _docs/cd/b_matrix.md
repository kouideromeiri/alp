---
title: B Matrix
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---


Sol taraftaki pillar seçeneklerinden CD-B Matris kısmına gelinir.

![BMatrix](/assets/img/CD/B_Matrix/bmatrix.png)

**Filtrele** butonu ile filtreleme kart ekranını açıp; istenilen alanlar seçilip, filtreleme yapılarak istenilen kayıtlara ulaşılabilir. Tarih aralığı ya da tek bir tarih girilerek kayıtlar listelenebilir. 

Sebep-sonuç kayıpları ilişkilendirildikçe; sistem bunu hafızasında tutar ve daha sonra benzer bir kayıp yaşandığında daha önceki kayıplara bakarak öneride bulunur sistemde buna Öğrenen B Matris ismi verilir. Böylece hem kayıpların birbiri ile sebep-sonuç ilişkisi sisteme öğretilir hemde sebep-sonuç ilişkisinden bugüne kadar ne kadar kayıp birbiri ile ilişkilendirilmiş bunun istatistiğine ulaşılabilir.

Ekranın sol tarafında bulunan **Öğrenen B Matris** butonuna tıklanıldığı zaman *ÖĞRENEN B MATRIS* sayfasına gidilir. Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurularak kaydedilir. Burada Bölüm ve Kayıp Tanımı olmak üzere iki tane zorunlu alan bulunur. Kaydetme işleminden sonra işlemin hangi sonuçları doğurabileceğinin tanımlandığı sonuç tablosu kısmı aktif hale gelecektir. (+) butonuna tıklanarak 'Sonuç Ekleme Dialoğu' tablosunda ilgili kısımlar doldurulur. Altı kırmızı renkli çizili alanlar zorunlu alanlardır. Yaşanan bir sebep kaybının, hangi sonuç kayıplarını ortaya çıkarabileceği buradan öğrenilebilir.

![BMatrix](/assets/img/CD/B_Matrix/bmatrix1.png)


Meydana gelen tüm kayıplar C matriste sistemden toplanır. Bu kayıpların bir tanesi sebep diğeri sonuç kaybıdır. Sonuç kayıplarının C matriste görülmemesi gerekiyor. (Tüm sonuç kayıplarının mutlaka bir sebebe bağlanıp C matriste ilgili sebep kaybı altında toplanarak.) Sistem oluşan tüm sonuç kayıplarını ilk olarak B matrise atıyor ve bu kısımda kaybın sebebi nedir ilgili ilişkisinin kurulması isteniyor.

Örneklendirme adına; C Matris tarafında bir sebep-sonuç kaybı oluşturduk daha sonra kayıp buraya düşüyor.(400 TL'lik Fazla Mesai kaybı B matrise düşürüyor.) Yan tarafta bulunan küçük ok a tıklanıldığı zaman kaybın detayına ulaşılıyor. 'Sebep Kayba Bağla' butonuna tıklanıldığı zaman **Öneriler** kısmı Öğrenen B matriste (daha önceki öğrenilen verilere bakarak) son 48 saat içerisinde bu verilere benzer kayıp varsa onu öneri olarak getiriyor. Sistem öneride bulunmuyorsa **Tüm Veriler** kısmından **Filtreleme** ile, ilgili kayba ulaşabilir. İlgili satır seçilip 'Bağla' butonuna tıklanıldıktan sonra 'C Matrix bağlantısı yapılmıştır' mesajı geliyor. Sayfa yenilendikten sonra artık 'Fazla Mesai' yerine farklı bir kayıba bağlandığı için, bağlama onayı bekleyen sebep kaybı (ARIZA) olarak görülüyor ve bu kısımda ilgili bölüm sorumlusuna da mail atılıyor. ('Bağlamayı bekleyen kayıp onayı bekleniyor.' şeklinde.) Fazla Mesai nin kendisi ile ilgili detayları ve Arıza örneğinden gelen parasal çıktıların detayları da tabloda görülür. Kayıplar birbirine bağlandıkça bir ilişki oluşuyor. Yan taraftaki listede de ana sebep kaybını oluşturan tüm sebep-sonuç kayıplarının dağılımı yer alır. '?' butonuna tıklanıldığı zaman C Matrix Kayıp Bağlama kart ekranı açılır. Bu kısımda açıklama yapılıp 'Onay' butonuna tıklanıldığı zaman B matris listesinden bu kayıp yok olacaktır.

![BMatrix](/assets/img/CD/B_Matrix/bmatrix2.png)

![BMatrix](/assets/img/CD/B_Matrix/bmatrix3.png)

B matriste ilgili kayıp havuzunun yani listenin her zaman boş(temiz) olması gerekiyor. (Sistemdeki tüm sonuç veya sebep-sonuç kayıplarının bir sebep kaybına dönüşüp C matrisle entegre olarak.)

**Filtrele** butonu ile filtreleme kart ekranı açılıp; ilgili alanlarda filtreleme yapılarak istenilen kayıtlar listelenebilir.

*Geri* butonuyla *B MATRIS* ana sayfaya dönülür.




