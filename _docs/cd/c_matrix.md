---
title: C Matris
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-C Matris kısmına gelinir.

![CMatrix](/assets/img/CD/C_Matrix/cmatrix.png)

Burada ilk açılan sayfa *C Matris Analiz* sayfasıdır. En üstte kayıpların detaylı bir analizi bulunur. Bu ekranda farklı senaryolar(filtrelere göre) ile aynı verilerin farklı analizlerine ulaşılabilir. Analizinin bir alt kısmında bulunan tabloda aynı değerlerin sayısal analizleri yer alır ve burada bir hücreye tıklanırsa o kısımdaki kaybı oluşturan tüm toplam kayıp detayları aşağıdaki tabloda listelenir. **Filtrele** butonunun hemen yan tarafında bulunan **Liste** butonuna tıklandığı zaman *C MATRIS* sayfası açılır. C matris sisteme düşen her bir kaybın anlık olarak ve daha detaylı bir şekilde görülebileceği/yönetilebileceği yerdir.

**Filtrele** butonu ile filtreleme kart ekranını üzerinden filtreleme yapılarak istenilen kayıtlara ulaşılabilir. Burada 'Gruplama Kriteri' girişi zorunludur. Ekrandaki refresh(yenile) butonuyla atılan filtreye göre kayıpların listesine ulaşılabilir. Sol tarafta gelen küçük tabloda listelenmiş kayıtlardan birine tıklandığı zaman satır pembe renge boyanıp sağ tarafta o kayıpla ilgili tüm detay kayıplar listelenecektir. 

Yeşil renkli **Ekle** butonu ile gelen ekranda ilgili kısımlar doldurularak kaydedilir. Altı kırmızı renkli çizili alanlar zorunlu alanlardır. Bu kısımda Alt Kayıp kısmına -Fazla Mesai(sonuç kaybı)- eklemesi yaparak örneklendirdik. Bir alt kısımda bulunan 'Finansal Çıktılar' tablosunda ilgili kısımları doldurduk. (Bu kısımda -Direk İşçilik- seçerek, -400 TL'lik- bir kayıp tanımladık.) Ekranın sağ tarafında bulunan Güncelleme Geçmişi kısmının gelmesi için 'Onaylı' tik inin seçilmesi ve Ekle butonunun aktif olabilmesi için kaydetmek gerekir.

![CMatrix](/assets/img/CD/C_Matrix/cmatrix1.png)

Yeni bir kayıt daha ekliyoruz. Bu kısımda Alt Kayıp kısmına -Elektronik Arıza(sebep kaybı)- eklemesi yaparak örneklendirdik. Tekrar bir alt kısımda bulunan 'Finansal Çıktılar' tablosunda ilgili kısımları doldurduk. (Bu kısımda -300 TL'lik- bir -Endirekt Malzeme Kaybı- ve -100 TL'lik- bir -Endirek İşçilik- kayıpları tanımladık.) Ekranın sağ tarafında bulunan Ekle butonu kaydetme işleminden sonra aktif hale gelecektir. Ekle butonu ile istenilen belgeler sisteme eklenebilir. 'Onay' tik i seçilerek kaydetme işlemi gerçekleştirildiği için Güncelleme Geçmişi görülmektedir.

![CMatrix](/assets/img/CD/C_Matrix/cmatrix2.png)

C matriste (B matris kayıp bağlama işleminden sonra) fazla mesai yok olup ilgili arıza kaybının toplamının üzerine eklendi (-400 TL'lik- Fazla Mesai ve -400 TL'lik- Arıza olmak üzere iki ayrı kayıp olarak gösteriliyordu. Fakat bağlama işleminden şuan artık -800 TL'lik- ARIZA olarak görülüyor. ) (>) sembolüne tıklanarak detaya ulaşılabilir. Detayın sağ tarafında kaybın nereden geldiğini ve o kısımda bulunan (>) sembolü ile de detay kısmında onayı kimin verdiği ve açıklamasına ulaşılabilir. İlgili kayıp satırının sonunda çöp kutusu sembolü bulunur. Sembole tıklanıldığı zaman; kaybın kendisini değil, kaybın bağlantısını silinir.

![CMatrix](/assets/img/CD/C_Matrix/cmatrix3.png)

**Analiz** butonuna tıklanıldığı zaman ilk açılıştaki analiz sayfasına ulaşılır. **Liste** butonu ile *C MATRIS* ana sayfasına geri dönülür.

Anasayfada **Ay Kapanışı** butonuna tıklanıldığı zaman *C Matris Ay Kapanışı* sayfasına gidilir. Burada yeşil renkli **Ekle** butonu ile gelen ekranda yıl ve ay kısımları doldurularak ay kapatılabilir. İlgili ay kapatıldığında sistem kapatılan aydaki C matrisi aynen kapandığı şeklinde korur var olan bir kaybın güncellenmesi ya da yeni bir kaydın girişine izin vermez.  *Geri* butonuyla *C MATRİS* ana sayfaya dönülür.
