---
title: F Matris
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-F Matris kısmına gelinir.

![FMatrix](/assets/img/CD/F_Matrix/fmatrix.png)

FI modülü tarafında kaizen projeleri kapandıkça F matris otomatik olarak güncellenir.

Planlanan(P) net kazanç ve Gerçekleşen(R) net kazanç sistemde aynı hücre içerisinde yer alırlar. İlgili hücreye tıklandığı zaman, detay kartı açılarak detayı görülür. Kaizen sahada yaşamıyorsa ilgili proje ve ayın kesiştiği hücreye tıklanarak açılan detay kısmında 'Durdur' butonuna tıklanıldığı zaman;  açılan 'F Matris Durdurma Dialoğu' kart ekranında açıklama girilerek kaizen durdurulur. (Kaizenin ilgili ay ve sonrasında kazanç üretmesi durdurulmuş olur.) Bu işlemden sonra gerçekleşen değerleri 0 olur. Belirli aylar için durdurulup; ilgili çalışma tekrar yapılmak istenirse tekrar başlatılarak, projenin kazanç üretmesi sağlanabilir. Durdurma işlemi kaizenin iptali anlamına gelmez. (Kaizenin hiç hayata geçirilmemesi ve çalışmasının başlatılmaması durumunda kaizen iptal olmuş olur.)

![FMatrix](/assets/img/CD/F_Matrix/fmatrix1.png)

İlgili proje ve ayın kesiştiği hücrenin yan tarafında bulunan '...' sembolüne tıklanarak; kayıp ve finansal çıktılarla ilgili detay ekranına ulaşılabilir.

![FMatrix](/assets/img/CD/F_Matrix/fmatrix3.png)

F matris; ilgili aydaki kaizenleri tarayarak hesaplamasını yapar. Satırın üzerine gelindiğinde kaizenle alakalı değişiklik yapılıp (kazanç değişikliği) satır yenilenirse  satırı günceller. Ay isimlerinin üzerinde bulunan refresh(yenile) butonu ile o ayın bulunduğu tüm sütunu günceller.

![FMatrix](/assets/img/CD/F_Matrix/fmatrix2.png)

Satırların başlarında bulunan linklere tıklanıldığı zaman ilgili E Matrix ve Kaizen sayfaları yan sekmede açılacaktır.

### Kaizenlerin F Matrise Yansımaları
#### E Matrix Kaizeni
E Matrix kaizenlerinde planlanan değer; kaizenin kapandığı yıl(2019) için E Matris'ten gelir (Kazanç). Kaizeni dikkate almaz.

Gelecek yıl için ise(2020) planlanan değer; kaizenden gelir (Kazanç/Finansal Çıktılar). Aşağıdaki tablolarda basitçe örneklendirilmiştir.

 E Matris |  Kaizen  |        
----------|----------|
   1500   |   1800   |   

|              |     2019      |    2020     | 
---------------|---------------|------------ |  
 Planlanan(P)  |     1500      |    1800     |    
 Gerçekleşen(G)|     1800      |             |    

![EMatrix](/assets/img/CD/F_Matrix/fmatrix4.png)

![Kaizen(EMatrix)](/assets/img/CD/F_Matrix/fmatrix5.png)

![Kaizen(EMatrix)](/assets/img/CD/F_Matrix/fmatrix5k.png)

F Matris tarafında; kaizenin kapandığı yıl olan 2019 için planlanan değer E Matris tarafında herhangi bir girdi bulunmadığı için 0 olarak gelmiştir. Yine 2019 gerçekleşen ise;  kaizenden gelmiştir. Aşağıdaki görselde görülen gerçek değer, sadece projenin kapandığı Aralık ayına yansımıştır.(2400:12=200,200-800(maliyet=-600))

![FMatrix/19](/assets/img/CD/F_Matrix/fmatrix6.png)

Yine F Matris tarafında; bir sonraki yıl(2020) için planlanan değer kaizenden gelmiştir. Aşağıdaki görselde görüldüğü üzere, planlanan kazanç değeri 11 aya eşit olarak bölünerek yansımıştır. (Kazanç ay sayısı 12 ay olarak planlanmıştır, Aralık ayı haricinde bu yıl içerisindeki 11 aya 200 olarak planlanan değerler atanır.)

![FMatrix/20](/assets/img/CD/F_Matrix/fmatrix7.png)

Kaizenin kazancı hangi kaybı iyileştirdiği ile alakalıdır. Üretime bağlı bir değişken(NVAA) ise; üretim sayısını alır ve bir formüle göre hesaplayıp gerçekleşen kısmına getirir. 

![Kaizen/NVAA](/assets/img/CD/F_Matrix/fmatrix8.png)

![Kaizen/NVAA](/assets/img/CD/F_Matrix/fmatrix9.png)

F Matris tarafında; kaizenin kapandığı yıl olan 2019 için NVAA kaybı iyileştirilmiştir ve değer gerçekleşen kısmına yansımıştır. Aşağıdaki görselde görülen gerçek değer, sadece projenin kapandığı Aralık ayına yansımıştır.

![FMatrix/19](/assets/img/CD/F_Matrix/fmatrix10.png)

Yine F Matris tarafında; bir sonraki yıl(2020) için planlanan değer kaizenden gelmiştir. Aşağıdaki görselde görüldüğü üzere, planlanan kazanç değeri 4 aya eşit olarak bölünerek yansımıştır. (Kazanç ay sayısı 5 ay olarak planlanmıştır, Aralık ayı haricinde bu yıl içerisindeki 4 aya 200 olarak planlanan değerler atanır.)

![FMatrix/20](/assets/img/CD/F_Matrix/fmatrix11.png)

#### Spontan Kaizen
Spontane kaizen ilgili yıl için sadece gerçekleşen oluşturur. Planı arttırmaz. Gelecek yıl için plan oluşturur.

![Kaizen/Spontan](/assets/img/CD/F_Matrix/fmatrix12.png)

![Kaizen/Spontan](/assets/img/CD/F_Matrix/fmatrix13.png)

F Matris tarafında; kaizenin kapandığı yıl olan 2019 için kaizenden gerçekleşen oluşmuştur. Aşağıdaki görselde görülen gerçek değer, sadece projenin kapandığı Aralık ayına yansımıştır.(2400:4=600,600-120=480)

![FMatrix/19](/assets/img/CD/F_Matrix/fmatrix14.png)

Yine F Matris tarafında; bir sonraki yıl(2020) için planlanan değer kaizenden gelmiştir. Aşağıdaki görselde görüldüğü üzere, planlanan kazanç değeri 3 aya eşit olarak bölünerek yansımıştır. (Kazanç ay sayısı 4 ay olarak planlanmıştır, Aralık ayı haricinde bu yıl içerisindeki 4 aya 600 olarak planlanan değerler atanır.)

![FMatrix/20](/assets/img/CD/F_Matrix/fmatrix15.png)

##### Not I :
İleri tarih için reel/gerçekleşen hesaplanamaz.
##### Not II: 
Kaizenin toplam kazancı kaizen toplam kazanç ay sayısına istinaden girilmelidir.








