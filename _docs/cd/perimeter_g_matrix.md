---
title: Perimeter / G Matris
subtitle: Kurulum aşamalarını aşağıdaki gibi yapabilirsiniz.
author: ayse
tags: [setup]
---

Sol taraftaki pillar seçeneklerinden CD-Perimeter / G Matris kısmına gelinir.

![GMatrix](/assets/img/CD/PerimeterG_Matrix/perimeter.png)

Ekrandaki refresh(yenile) butonuyla Perimeter listesine ulaşılabilir. **Filtrele** butonu ile istenilen yılın kayıpları filtrelenebilir. 

Transformasyon verileri bir sonraki seneyi planlarken Perimeter oluşturmada kullanılır. (Örn: 2019 yılı için Perimeter hesaplarken sistem 2018 yılının tüm perimeter verilerini alacaktır.) Sistem sağ tarafta oluşan gider toplamlarını getiriyor. Gelen Transformasyon verilerinde bir değişiklik gözlemleniyorsa Değişim kısımında (+/- yönde) sisteme tanımlanır ve sistem bir toplam Volume/Mix Etkisi hesaplar. 

![GMatrix](/assets/img/CD/PerimeterG_Matrix/perimeter1.png)

![GMatrix](/assets/img/CD/PerimeterG_Matrix/perimeter2.png)

![GMatrix](/assets/img/CD/PerimeterG_Matrix/perimeter3.png)

Ekstra Fenomen oluşması bekleniyorsa miktarı ve açıklaması 'Ekstra Fenomen' tablosuna girilir ve ekranın sol tarafına girilen verinin gösterimi gelir. Bu kısımda toplam Perimeter değeri ortaya çıkar ve bu noktada getiri hedefi oranı girilerek toplam Perimeter değeri üzerinden getiri toplamı oluşur. 

![GMatrix](/assets/img/CD/PerimeterG_Matrix/perimeter4.png)

![GMatrix](/assets/img/CD/PerimeterG_Matrix/perimeter5.png)

Bunun üzerine gider kalemi olarak beklenen enflasyon öngörüsü girilir. Yine sistemin sol tarafında enflasyon oranı ve G Matris öngörüsü gelecektir. Bu sonuçlara bağlı olarakta 'Waterfall Chart Güncelle' butonuna tıklanıldığı zaman Waterfall şeması otomatik oluşur.

Kaydetme işlemi gerçekleştikten sonra belirlenen getiri oranındaki gelir toplamını (üzerinde çalışılacak olan E matris projelerindeki hedef) gerçekleştirmek  adına sisteme bir kayıt açılıyor ve E matris bu hedefe ulaşmak için çalışmaya başlar.



